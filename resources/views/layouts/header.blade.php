<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                <span class="sr-only">{{ __('Toggle navigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{ Html::link(url('/'), __('Next Front System / ADMIN'), ['class' => 'navbar-brand']) }}
        </div>
        @auth
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ __('Menu') }}<i class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            {{ Html::link(route('users.index'), __('Quản lý người dùng')) }}
                        </li>
                    </ul>
                </li>
                <li class="divider hidden-xs">
                    {{ Html::link('#', __('|', ['onfocus'=>'javascript:blur();'])) }}
                </li>
                <li>
                    {{ HTML::link(route('logout'), 'Đăng xuất', array('onclick' => "event.preventDefault();
                                 document.getElementById('logout-form').submit();"))}}
                    {{ Form::open(['url' => route('logout'),
                        'method' => 'POST',
                        'style' => 'display: none',
                        'id' => 'logout-form']) }}
                    {{ Form::close() }}
                </li>
            </ul>
        </div>
        @endauth
    </div>
</div>