@extends('layouts.main')
@section('title', __('ADMINユーザ管理'))
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="btn-link panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{{ __('▼　ユーザーを編集する') }}</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {{ Form::model($user, ['method' => 'POST', 'class' => 'form-horizontal']) }}
            {{ Form::text('urlPrevious', url()->previous(), ['class' => 'hidden']) }}
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <div class="col-sm-2">
                    {!! Html::decode(Form::label('inputName', __('ユーザ名') . '<span class="red">*</span>', ['class' => 'control-label'])) !!}
                </div>
                <div class="col-sm-10">
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => __('例：　クレアンス太郎'), 'maxlength' => 100]) }}
                    <br>
                    @if ($errors->has('name'))
                        <div class="text-danger">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <div class="col-sm-2">
                    {!! Html::decode(Form::label('inputEmail', __('Eメールアドレス') . '<span class="red">*</span>', ['class' => 'control-label'])) !!}
                </div>
                <div class="col-sm-10">
                    {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => __('例：　creans.taro@creansmaerd.co.jp'), 'maxlength' => 100]) }}
                    <br>
                    @if ($errors->has('email'))
                        <div class="text-danger">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <div class="col-sm-2">
                    {{ Form::label('inputPassword', __('パスワード'), ['class' => 'control-label']) }}
                </div>
                <div class="col-sm-10">
                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => __('********'), 'id' => 'inputPassword', 'maxlength' => 50]) }}
                    <br>
                    @if ($errors->has('password'))
                        <div class="text-danger">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                <div class="col-sm-2">
                    {{ Form::label('inputPasswordConfirmation', __('パスワード確認'), ['class' => 'control-label ']) }}
                </div>
                <div class="col-sm-10">
                    {{
                        Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => __('********'), 'id' => 'inputPasswordConfirmation', 'maxlength' => 50])
                    }}
                    <br>
                    @if ($errors->has('password_confirmation'))
                        <div class="text-danger">
                            {{ $errors->first('password_confirmation') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    {{ Form::submit(__('保存'), ['class' => 'btn btn-default']) }}
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection